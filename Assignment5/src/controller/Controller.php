<?php

include_once("src/model/Model.php");
include_once("src/view/View.php");


class Controller {
	public $xml, $db, $view;					// Public variables
	
	
	public function __construct() {				// Creates new objects
		$this->xml = new DOMmodel;				// The DOMmodel object
		$this->db = new DBModel;				// The DBModel object
		$this->view = new View;					// The view object
	}
	
	public function invoke() {
		$clubs = $this->xml->getClubs();		// Gets clubs
		$this->db->addClubs($clubs);			// Adds clubs to the database
		$skiers = $this->xml->getSkiers();		// Gets skiers
		$this->db->addSkiers($skiers);			// Adds clubs to the database
		$seasons = $this->xml->getSeasons();	// Gets seasons
		$this->db->addSeasons($seasons);		// Adds seasons to the database
	}
}
?>