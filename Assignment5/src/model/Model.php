<?php

/*Including classes */
include_once("Skier.php");
include_once("Club.php");
include_once("Season.php");

/*Accessing the XML-data with the DOM-api */
class DOMmodel {
	protected 
		$xml = null;
	
	public function __construct($xml = null) {
		if ($xml) {									//if the xml-file exist 
			$this->xml = new DOMDocument();			
			$this->xml->load($xml);
		}
		else {
			$this->xml = new DOMDocument();
			$this->xml->load('SkierLogs.xml');		// Loads the specific file if it doesn't exist
		}
	}
	
	/* Getting info about skiers */
	public function getSkiers() {
		$skiersList = array();
		$xpath = new DOMXpath($this->xml);
		$skiers = $xpath->query('/SkierLogs/Skiers/Skier');										// Read data from 'Skier'
		
		foreach($skiers as $skier) {															// Loops through the skiers.
			$userName = $skier->getAttribute('userName');										// Gets the username
			$firstName = $skier->getElementsByTagName('FirstName')->item(0)->textContent;		// Gets firstname
			$lastName = $skier->getElementsByTagName('LastName')->item(0)->textContent;			// Gets lastName
			$yearOfBirth = $skier->getElementsByTagName('YearOfBirth')->item(0)->textContent;	// Gets the skiers year of birth
			
			$skiersList[] = new Skier($userName, $firstName, $lastName, $yearOfBirth);			// Adds the data about the skier
		}
		return $skiersList;																		// Returns the complete list of skiers
	}
	
	/* Getting info about each club */
	public function getClubs() {
		$clubsList = array();
		$xpath = new DOMXpath($this->xml);
		$clubs = $xpath->query('//SkierLogs/Clubs/Club');										// Read data from 'Club'
		
		foreach($clubs as $club) {																// Loops through the clubs
			$id = $club->getAttribute('id');													// Gets club ID
			$name = $club->GetElementsByTagName('Name')->item(0)->textContent;					// Gets club name
			$city = $club->GetElementsByTagName('City')->item(0)->textContent;					// Gets city name
			$county = $club->GetElementsByTagName('County')->item(0)->textContent;				// Gets county name
			
			
			$clubsList[] = new Club($id, $name, $city, $county);								// Adds the data about the club
		}
		return $clubsList;																		// Returns the complete list of clubs
	}	

	/* Getting info about each season*/
	public function getSeasons() {
		$seasonsList = array();
		$xpath = new DOMXpath($this->xml);
		$seasons = $xpath->query('//SkierLogs/Season');
		
		foreach($seasons as $season) {
			$fallYear = $season->getAttribute('fallYear');											// Gets the fallYear 
				foreach($season->getElementsByTagName('Skiers') as $skiers) {						// Loops through skiers 
					$clubId = $skiers->getAttribute('clubId');										// Gets the club ID 
						foreach($skiers->getElementsByTagName('Skier') as $skier) {					// Loops through Skier 
							$totalDistance = 0;														// Totaldistance is set to zero 
							$userName = $skier->getAttribute('userName');							// Gets the skier's username 
								foreach($skier->getElementsByTagName('Distance') as $distance) {	// Loops through the skier's entries to find his total distace that season 
									$totalDistance += $distance->nodeValue;							// Adds distance to the sum for each entry 
						}
						$seasonsList[] = new Season($userName, $fallYear, $clubId, $totalDistance);	// Sends the data to the constructor that initialises the data for each season 
					}		
				}
			}
			return $seasonsList;																	// Returns the list of seasons 
		}
}

// Class for the DB-model:
class DBModel {
	
	protected $db = null;
	
	/* Creates PDO-link to the database "assignment5" */
	public function __construct($db = null) {
		try {																						// Try-catch if something goes wrong
			if($db) {
				$this->db = $db;
			}
			else {
				$this->db = new PDO('mysql:host=localhost;dbname=assignment5;', 'root', ''); 		// Using PDO to establish a link to the DB
			}
		} catch (PDOEXCEPTION $pdoe) {
			echo 'Something went wrong!' .$pdoe->getMessage();
		}
	}
	
	/* Adds skiers to the database */
	public function addSkiers($skiersList) {
		try {																										// Try-catch if something goes wrong
			foreach($skiersList as $skier) {																		// Loops through the skierslist one skier at a time
				$stmt = $this->db->prepare("INSERT INTO skiers(userName, firstName, lastName, yearOfBirth)			
				VALUES(?,?,?,?)");																					// Using prepared statements to avoid sql-injections
				$stmt->execute(array($skier->userName, $skier->firstName, $skier->lastName, $skier->yearOfBirth));	// Executes array
			}
			
		} catch (PDOEXCEPTION $pdoe) {
			echo 'Something went wrong!' .$pdoe->getMessage();
		}
	}
	
	/* Adds clubs to the database */
	public function addClubs($clubsList) {
		try {
			foreach($clubsList as $club) {																			// Try-catch if something goes wrong
				$stmt = $this->db->prepare("INSERT INTO club(id, cname, cityName, county)
				VALUES(?,?,?,?)");																					// Using prepared statements to avoid sql-injection
				$stmt->execute(array($club->id, $club->name, $club->city, $club->county));							// Executes array.
			}
			
		} catch (PDOEXCEPTION $pdoe) {
			echo 'Something went wrong!' .$pdoe->getMessage();
		}
	}
	
	/* Adds seasons to the database */
	public function addSeasons($seasonsList) {
		try {																										// Try-catch if something goes wrong
			foreach($seasonsList as $season) {																		// Loops through the seasonslist one season at a time
				$stmt = $this->db->prepare("INSERT INTO skierclubseason(userName, fallYear, clubId, totalDistance)
				VALUES(?,?,?,?)");																					// Using prepared statements to avoid sql-injections
				if($season->clubId == null) {																		// If the skier has no club
					$stmt->execute(array($season->userName, $season->fallYear, null, $season->totalDistance));		// clubId = null
				} else {
				$stmt->execute(array($season->userName, $season->fallYear, $season->clubId, $season->totalDistance)); // If skier is member of a club.
				} 
			}
			
		} catch (PDOEXCEPTION $pdoe) {
			echo 'Something went wrong!' .$pdoe->getMessage();
		}
	}
	
}

?>
