<?php

class Skier {
	public $userName;		// Public variables
	public $firstName;
	public $lastName;
	public $yearOfBirth;
	
	public function __construct($userName, $firstName, $lastName, $yearOfBirth) {
		$this->userName = $userName;		// Initiates username
		$this->firstName = $firstName;		// Initiates firstname
		$this->lastName = $lastName;		// Initiates lastname
		$this->yearOfBirth = $yearOfBirth;	// Initiates yearOfBirth
	}
}

?>