<?php

class Club {
	public $id;				// Public variables for club
	public $name;
	public $city;
	public $county;
	
	public function __construct($id, $name, $city, $county) {
		$this->id = $id;				// Initiates club id
		$this->name = $name;			// Initiates club name
		$this->city = $city;			// Initiates city
		$this->county = $county;		// Initiates county
	}
}
?>