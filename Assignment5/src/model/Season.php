<?php

class Season {
	public $userName;		// Public variables
	public $fallYear;
	public $clubId;
	public $totalDistance;
	
	public function __construct($userName, $fallYear, $clubId, $totalDistance) {
		$this->userName = $userName;			// Initiates username
		$this->fallYear = $fallYear;			// Initiates fallYear
		$this->clubId = $clubId; 				// Initiates club ID
		$this->totalDistance = $totalDistance;	// Initiates totalDistance
	}
}

?>