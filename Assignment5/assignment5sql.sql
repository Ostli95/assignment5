DROP SCHEMA IF EXISTS assignment5;
CREATE SCHEMA assignment5 COLLATE = utf8_general_ci; 

USE assignment5;

/* Creating the skiers table */
create table Skiers (
	userName		varchar(10)		not null,
	firstName		varchar(30)		not null,
	lastName		varchar(30)		not null,
	yearOfBirth		int(4)			not null,
	primary key (userName)
);

/* Creating the club table */
create table Club (
	id				varchar(30)		not null,
	cname			varchar(20)		not null,
	cityName		varchar(20)		not null,
	county			varchar(20)		not null,
	primary key (id)
);

/* Creating the SkierClubSeason table which has clubId and userName as foreign keys */
create table SkierClubSeason (
	userName		varchar(10)		not null,
	fallYear		int(4)			not null,
	clubId			varchar(30),
	totalDistance	int(11),
	primary key (userName, fallYear),
	foreign key (userName) 			references Skiers(userName),
	foreign key (clubId) 			references Club(id)
);


